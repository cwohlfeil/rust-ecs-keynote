pub struct AnyMap { ... }

impl AnyMap {
    pub fn insert<T>(&mut self, t: T) { ... }
    pub fn get<T>(&mut self) -> Option<&T> { ... }
    pub fn get_mut<T>(&mut self) -> Option<&mut T> { ... }
}

struct PhysicsComponent { ... }
struct HumanoidAnimationComponent { ... }
struct HumanoidItemsComponent { ... }
struct MonsterAnimationComponent { ... }
struct NpcBehaviorComponent { ... }
struct AggressionComponent { ... }
struct HealthComponent { ... }
struct HungerComponent { ... }
struct PlayerComponent { ... }

type Entity = GenerationalIndex;
type EntityMap<T> = GenerationalIndexArray<T>;

struct ECS {
    entity_allocator: GenerationalIndexAllocator,
    
    // we're assuming that this will contain only types of the pattern `EntityMap<T>`, this is dynamic so the type system stops being helpful
    entity_components: AnyMap,
    
    // non-entity state data
    resources: AnyMap,
}

impl ECS {
    fn get_component<T: Component>(&self) -> Option<&EntityMap<T>> { ... }
    fn get_component_mut<T: Component>(&mut self) -> Option<&mut EntityMap<T>> { ... }
    
    fn get_resource<T: Resource>(&self) -> Option<&T> { ... }
    fn get_resource_mut<T: Resource>(&mut self) -> Option<&mut T> { ... }
}