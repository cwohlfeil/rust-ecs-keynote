pub struct ComponentRegistry { ... }

impl ComponentRegistry {
    // Registers a component, components must implement a special trait to allow e.g. loading from a json config.
    pub fn register_component<T: Component>(&mut self) { ... }

    // Sets up entries for all registered components to the given ECS
    pub fn setup_ecs(&self, ecs: &mut ECS) { ... }

    // Loads a given entity into the given ECS, loading all the components from the given JSON
    pub fn load_entity(&self, json: Json, ecs: &mut ECS) -> Entity { ... }
}

pub struct ResourceRegistry { ... }

impl ResourceRegistry {
    // The Resource trait provides loading from json and other things.
    pub fn register_resource<T: Resource>(&mut self) { ... }

    // Sets up entries for all registered resources to the given ECS
    pub fn setup_ecs(&self, ecs: &mut ECS) { ... }

    // Adds a resource to the given ECS by loading from the given JSON.
    pub fn load_resource(&self, json: Json, ecs: &mut ECS) { ... }
}

fn load_component_registry() -> ComponentRegistry {
    let mut component_registry = ComponentRegistry::new();

    component_registry.register::<PhysicsComponent>();
    component_registry.register::<PlayerComponent>();
    ...
}

fn load_resource_registry() -> ResourceRegistry {
    let mut resource_registry = ResourceRegistry::new();

    resource_registry.register::<BlocksResource>();
    ...
}

pub struct Registry {
    pub components: ComponentRegistry,
    pub resources: ResourceRegistry,
}

lazy_static! {
    pub static ref REGISTRY: Registry = Registry {
        components: load_component_registry(),
        resources: load_resource_registry(),
    };
}
