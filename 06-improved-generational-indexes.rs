struct ArrayEntry<T> {
    value: T,
    generation: u64,
}

// an associative array from GenerationalIndex to some Value T
pub struct GenerationalIndexArray<T> {
    // set the value for some generational index. mat overwrite past generation values
    pub fn set(&mut self, index: GenerationalIndex, value: T) { ... }
    
    // Gets the value for some generational index, the generation must match
    pub fn get(&self, index: GenerationalIndex) -> Option<&T> { ... }
    pub fn get_mut(&mut self, index: GenerationalIndex) -> Option<&mut T> { ... }
}