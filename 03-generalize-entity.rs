type usize = EntityIndex;

struct Physics { ... }
struct HumanoidState { ... }
struct MonsterAnimationState { ... }
struct DamageRegion { ... }
struct NpcBehavior { ... }

struct Aggression {
    current_target: EntityIndex,
}

struct Health(f32);

struct Hunger {
    food_level: f32,
}

struct PlayerState {
    focused_entity: EntityIndex,
    admin: bool,
}

struct MonsterState {
    current_target: EntityIndex,
    animation_state: MonsterAnimationState,
}

struct NpcState {
    behavior: NpcBehavior,
}

struct Entity {
    physics: Option<Physics>,
    humanoid_state: Option<HumanoidState>,
    monster_animation: Option<MonsterAnimationState>,
    npc_behavior: Option<NpcBehavior>,
    aggression: Option<Aggression>,
    health: Option<Health>,
    hunger: Option<Hunger>,
    player: Option<PlayerState>,
    ...
}

struct GameState {
    entities: Vec<Option<Entity>>,
    players: Vec<EntityIndex>,
    ...
}