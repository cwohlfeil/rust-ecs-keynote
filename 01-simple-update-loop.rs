struct Player { ... }
struct Monster { ... }
struct Npc { ... }

enum Entity {
    Player(Player),
    Monster(Monster),
    Npc(Npc),
}

struct GameState {
    entities: Vec<Option<Entity>>,
    players: Vec<EntityIndex>,
    ...
}

fn main() {
    let mut game_state = initial_game_state();
    
    loop {
        let input_state = capture_input_state();
        player_control_system(&mut game_state, &input_state);
        npc_behavior_system(&mut game_state);
        monster_behavior_system(&mut game_state);
        
        physics_system(&mut game_state);
        
        // ...
        
        render_system(&mut game);
        audio_system(&mut game);
        
        wait_vsync();
    }
}