type usize = EntityIndex;

struct Physics { ... }
struct HumanoidState { ... }
struct MonsterAnimationState { ... }
struct DamageRegion { ... }
struct NpcBehavior { ... }

struct PlayerState {
    focused_entity: EntityIndex,
    food_level: f32,
    admin: bool,
}

struct MonsterState {
    current_target: EntityIndex,
    animation_state: MonsterAnimationState,
}

struct NpcState {
    behavior: NpcBehavior,
}

struct Entity {
    physics: Option<Physics>,
    health: Option<f32>,
    humanoid: Option<HumanoidState>,
    player: Option<PlayerState>,
    monster: Option<MonsterState>,
    npc: Option<NpcState>,
    ...
}

struct GameState {
    entities: Vec<Option<Entity>>,
    players: Vec<EntityIndex>,
    ...
}